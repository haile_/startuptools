var path                = require('path');

exports.setupContent = setupContent;

function setupContent(webServer) {
  var p = webServer.baseProvider.copy();

  webServer.setUrl('/pong/bundle.js', p.addBrowserify(require.resolve('./pongmain.js')));

  p.setTitle('Pong!');
  webServer.setupStdContent('/pong/');
  webServer.setPrefixHosts('/pong/', [
    'pong.tlb.org',
  ]);
  webServer.setUrl('/pong/', p);
}
