const events = require('events');

exports.PongModel = PongModel;

function PongModel(o) {
  var m = this;
  m.leftPaddle = 0.5;
  m.rightPaddle = 0.5;
  m.ballX = 0.5;
  m.ballY = 0.5;
  m.ballVelX = 0.5;
  m.ballVelY = 0.2;
  m.leftPaddleMove = 0;
}

PongModel.prototype = Object.create(events.EventEmitter.prototype);


PongModel.prototype.animate = function(dt) {
  var m = this;

  m.leftPaddle += m.leftPaddleMove * dt;
  if (m.leftPaddle > .95) m.leftPaddle = .95;
  else if (m.leftPaddle < .05) m.leftPaddle = .05;
  
  m.ballX += m.ballVelX * dt;
  m.ballY += m.ballVelY * dt;

  if (m.ballY > 0.95) {
    m.ballVelY = -Math.abs(m.ballVelY);
    m.ballY = 0.95;
  }
  else if (m.ballY < 0.05) {
    m.ballVelY = +Math.abs(m.ballVelY);
    m.ballY = 0.05;
  }


  if (m.ballX > 0.95) {
    m.ballVelX = -Math.abs(m.ballVelX);
    m.ballX = 0.95;
  }
  else if (m.ballX < 0.05) {
    m.ballVelX = +Math.abs(m.ballVelX);
    m.ballX = 0.05;
  }

  m.emit('changed');
};
