
'use strict';
const _ = require('lodash');
const {$} = require('tlbcore/web/vjs_browser');
const pongmodel = require('./pongmodel');
const canvasutils = require('tlbcore/web/canvasutils');

$.defPage('', function(o) {
  var top = this;
  var m = new pongmodel.PongModel(o);

  top.html('<div class="pongView"><canvas class="pongCanvas" width="500" height="500"></canvas></div>');

  top.find('.pongCanvas').mkAnimatedCanvas(m, drawPong, {});

  top.animation2(m);
  m.emit('changed');

  top.children().first().bogartBodyEvents({
    'keydown': onKeydown,
    'keyup': onKeyup,
  });
  
  function onKeydown(ev) {
    console.log('Down', ev.which);
    if (ev.which == 81) {
      m.leftPaddleMove = -1;
    }
    else if (ev.which == 65) {
      m.leftPaddleMove = +1;
    }
  }
  function onKeyup(ev) {
    console.log('Up', ev.which);
    if (ev.which == 81 || ev.which == 65) {
      m.leftPaddleMove = 0;
    }
  }

  return this;
});


function drawPong(m, ctx, hd, lo, o) {
  setupLayout();
  drawPaddles();
  drawBall();
  drawBox();

  function setupLayout() {
    
  }
  function drawPaddles() {
    
    if (0) console.log('lo.boxT=',lo.boxT,'lo.boxB=',lo.boxB,'lo.boxL=',lo.boxL,'lo.boxR=',lo.boxR);

    var paddleY = lo.boxT + (lo.boxB - lo.boxT) * m.leftPaddle;
    var paddleT = paddleY - (lo.boxB - lo.boxT) * 0.05;
    var paddleB = paddleY + (lo.boxB - lo.boxT) * 0.05;

    var paddleL = lo.boxL + 10;
    var paddleR = lo.boxL + 30;

    ctx.fillStyle = '#ff0000';
    ctx.beginPath();
    ctx.moveTo(paddleL, paddleT);
    ctx.lineTo(paddleR, paddleT);
    ctx.lineTo(paddleR, paddleB);
    ctx.lineTo(paddleL, paddleB);
    ctx.lineTo(paddleL, paddleT);
    ctx.fill();



    
  }
  function drawBall () {

    var ballY = lo.boxT + (lo.boxB - lo.boxT) * m.ballY;
    var ballX = lo.boxL + (lo.boxR - lo.boxL) * m.ballX;

    ctx.fillStyle = '#0000ff';
    ctx.beginPath();
    ctx.arc(ballX, ballY, (lo.boxR - lo.boxL) * 0.05, 0, 2*Math.PI);
    ctx.fill();
  }
  function drawBox () {

    ctx.strokeStyle = '#dd00dd';
    ctx.lineWidth = 3
    ctx.beginPath();
    ctx.moveTo(lo.boxL, lo.boxT)
    ctx.lineTo(lo.boxR, lo.boxT)
    ctx.lineTo(lo.boxR, lo.boxB)
    ctx.lineTo(lo.boxL, lo.boxB)
    ctx.lineTo(lo.boxL, lo.boxT)
    ctx.stroke();
  }
}

