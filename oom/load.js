var path                = require('path');

exports.setupContent = setupContent;

function setupContent(webServer) {
  var p = webServer.baseProvider.copy();

  p.addCss(require.resolve('./oom.css'));
  p.addCss(require.resolve('codemirror/lib/codemirror.css'));

  webServer.setUrl('/growth/bundle.js', p.addBrowserify(require.resolve('./oommain.js')));

  p.setTitle('Startup Growth Calculator');
  webServer.setupStdContent('/growth/');
  webServer.setPrefixHosts('/growth/', [
    'growth.tlb.org',
  ]);
  webServer.setUrl('/growth/', p);
}
