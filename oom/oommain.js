"use strict"
require('./oommodel.js');
require('./oomview.js');
require('codemirror/lib/codemirror.js');
require('codemirror/keymap/emacs.js');
require('codemirror/mode/javascript/javascript.js');
const vjs_browser = require('tlbcore/web/vjs_browser.js');
require('tlbcore/web/vjs_edit_url.js');


setTimeout(() => {
  vjs_browser.pageSetupFromHash();
});
