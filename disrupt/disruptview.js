'use strict';
const _ = require('lodash');
const {$} = require('tlbcore/web/vjs_browser');
const disruptmodel = require('./disruptmodel');
const canvasutils = require('tlbcore/web/canvasutils');

$.defPage('', function(o) {
  var top = this;
  var m = new disruptmodel.DisruptModel(o);

  top.html('<div class="disruptView"><canvas class="disruptCanvas" width="500" height="500"></canvas></div>');

  top.find('.disruptCanvas').mkAnimatedCanvas(m, drawDisrupt, {});

  top.animation2(m);
  m.emit('changed');

  top.children().first().bogartBodyEvents({
    'keydown': onKeydown,
    'keyup': onKeyup,
  });
  
  function onKeydown(ev) {
    console.log('Down', ev.which);
  }
  function onKeyup(ev) {
    console.log('Up', ev.which);
  }

  return this;
});
