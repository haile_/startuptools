const _ = require('lodash');
const events = require('events');

exports.DisruptModel = DisruptModel;

function SiteQuality(o) {
  var q = this;
  if (!o) o = {};
  q.hipsterWeight = (o.hipsterWeight !== undefined) ? o.hipsterWeight : 1.0;
  q.earlyWeight = (o.earlyWeight !== undefined) ? o.earlyWeight : 1.0;
  q.majorityWeight = (o.majorityWeight !== undefined) ? o.majorityWeight : 1.0;
  q.weirdoWeight = (o.weirdoWeight !== undefined) ? o.weirdoWeight : 1.0;
}

SiteQuality.prototype.uxAtScale = function(scale) {
  var q = this;
  

}


function DisruptModel(o) {
  var m = this;

  m.oldSite = new SiteQuality(o.oldSite);
  m.newSite = new SiteQuality(o.newSite);
}

DisruptModel.prototype = Object.create(events.EventEmitter.prototype);


DisruptModel.prototype.animate = function(dt) {
  var m = this;

  m.emit('changed');
};

