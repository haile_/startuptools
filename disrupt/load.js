var path                = require('path');

exports.setupContent = setupContent;

function setupContent(webServer) {
  var p = webServer.baseProvider.copy();

  p.addCss(require.resolve('./disrupt.css'));

  webServer.setUrl('/disrupt/bundle.js', p.addBrowserify(require.resolve('./disruptmain.js')));

  p.setTitle('Disrupt!');
  webServer.setupStdContent('/disrupt/');
  webServer.setPrefixHosts('/disrupt/', [
    'disrupt.tlb.org',
  ]);
  webServer.setUrl('/disrupt/', p);
}
